/*Nombre Pedro Joel Burgos
 * LU:804/18
 * */

#include <iostream>
#include <list>



using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10


class Fecha {
  public:
    Fecha(int mes, int dia);
    int mes();
    int dia();
    bool operator==(Fecha o);//9
    void incrementar_dia();//10
    bool operator!=(Fecha o);//14

  private:
    int mes_;
    int dia_;
    bool igual_dia;
    bool igual_mes;


};

Fecha::Fecha(int mes, int dia) :  mes_(mes) , dia_(dia) {
};

int Fecha::mes() {
    return mes_;
}


int Fecha::dia(){
    return dia_;
}
//8

ostream& operator<<(ostream& os, Fecha f) {
    os << "" << f.dia() << "/" << f.mes() << "";
    return os;
}

// 9
bool Fecha::operator==(Fecha o) {
    bool igual_dia = this-> dia() == o.dia();
    bool igual_mes = this-> mes() == o.mes();


    return igual_dia && igual_mes;
}





//10
void Fecha::incrementar_dia(){
    if(dias_en_mes(mes()) > dia())
    {
        dia_ = dia_ + 1;
    }
    else{
        mes_ = mes_ + 1;
        dia_ = 1;
    }
}





//11
class Horario {
public:
    Horario(uint hora, uint min);
    uint hora();
    uint min();
    bool operator<(Horario h);

private:
    uint hora_;
    uint min_;
};

Horario::Horario(uint hora, uint min):hora_(hora),min_(min){
}

uint Horario::hora(){
    return hora_;
}

uint Horario::min(){
    return min_;
}

ostream& operator<<(ostream& os, Horario h) {
    os << "" << h.hora() << ":" << h.min() << "";
    return os;
}



//12
bool Horario::operator<(Horario h) {
   return ((hora() == h.hora() && min()==h.min())
           || (h.hora() > hora())
           || (hora() == h.hora() && min()<h.min()));
}



// Ejercicio 13

class Recordatorio {
public:
    Recordatorio(Fecha fecha, Horario horario , string mensaje);
    Fecha fecha();
    Horario horario();
    string mensaje();


private:
    Fecha fecha_;
    Horario horario_;
    string mensaje_;
};

Recordatorio::Recordatorio(Fecha fecha, Horario horario , string mensaje): fecha_(fecha),horario_(horario), mensaje_(mensaje){
}

Fecha Recordatorio::fecha(){
    return fecha_;
}

Horario Recordatorio::horario(){
    return horario_;
}

string Recordatorio::mensaje(){
    return mensaje_;
}

ostream& operator<<(ostream& os, Recordatorio r) {
    os << "" << r.mensaje() << " @ " << r.fecha() << " " << r.horario();
    return os;
}




// Ejercicio 14

//Antes que nada arriba incluir el   #include <list>


class Agenda {
public:
    Agenda(Fecha fecha_inicial);
    void agregar_recordatorio(Recordatorio rec);
    void incrementar_dia();
    list<Recordatorio> recordatorios_de_hoy();
    Fecha hoy();

private:
    //siguiendo los ejemplos de ejercicios anteriores agrego recordatorios_de_hoy y hoy_
    list<Recordatorio> recordatorios_de_hoy_;
    Fecha hoy_;
};


Agenda::Agenda(Fecha fecha_incial ): hoy_(fecha_incial),recordatorios_de_hoy_(){
}

Fecha Agenda::hoy() {
    return hoy_;
}

//para agregar recordatorios pense en usar recordatoios_de_hoy y colorca ahi los de rec
void Agenda::agregar_recordatorio(Recordatorio rec) {
    recordatorios_de_hoy_.push_back(rec);
}

//Use la funcion del punto 10 incrementar_dia en hoy_
void Agenda::incrementar_dia() {
    hoy_.incrementar_dia();
}


bool comparaciondeHorarios (Recordatorio r , Recordatorio m) {
    //horario r debe ser menor a m para que sea true,utilizo la funcion que hice
    // en el ejercicio 12
    return  r.horario().operator<(m.horario());
}


list<Recordatorio> Agenda::recordatorios_de_hoy() {
    list <Recordatorio>::iterator it;

    for( it = recordatorios_de_hoy_.begin(); it != recordatorios_de_hoy_.end(); ){
        if( !(it->fecha().operator==(hoy()))  ){
            it = recordatorios_de_hoy_.erase(it);
        }else{
            it++;
        }
    }
    recordatorios_de_hoy_.sort(comparaciondeHorarios);
    return recordatorios_de_hoy_;
}




ostream& operator<<(ostream& os, Agenda a){
    os << a.hoy() << endl;
    os << "=====" << endl;

    list <Recordatorio> l = a.recordatorios_de_hoy();
    int longitud = l.size();

    for( int i=0; i<longitud;i++){

        os << l.front() << endl;
        l.pop_front();
    }
    return os;
}


